######################
#####DEPENDENCIES#####
######################

Default packages in Python v2.7.3.
Additional packages required (recommended install with pip)
- matplotlib
- pillow (or PIL)
- seaborn 


######################
######HOW TO RUN######
######################

To run a simulation, two input files are needed: the parameters (-paramfile) and the setup (-setupfile) file. These files parameterise the simulation run and define a bunch of logistic stuff. One of the most important is the first line in the parameters file, that defines  the output path of the simulations (relative to where the command is being run). Please be careful to end the path with the “/“ character.

The other mandatory argument is the ID (-id), that defines a name for the current simulation.

The flag —-visualize can be used for an interactive visualisation of the simulation, but it is not mandatory (it slows down the simulations).

The “Empties” folder needs to be located also in the folder where the run command is executed.

Run command:
python Handler.py -id <name> -paramfile <paramfile.txt> -setupfile <setupfile.txt> --visualize

Examples (with the included files)
(1) python src_eVIVALDI_9.2/Handler.py -id TestSimulation1 -paramfile TestScripts/FileS2_Fig2e.Parameters.txt -setupfile TestScripts/FileS1_Fig2e.MicrobialSetup.txt --visualize
(2) python src_eVIVALDI_9.2/Handler.py -id TestSimulation2 -paramfile TestScripts/FileS4_Fig4a.Parameters.txt -setupfile TestScripts/FileS3_Fig4a.MicrobialSetup.txt --visualize
(3) python src_eVIVALDI_9.2/Handler.py -id TestSimulation2 -paramfile TestScripts/FileS6_Fig5b.Parameters.txt -setupfile TestScripts/FileS5_Fig5b.MicrobialSetup.txt --visualize